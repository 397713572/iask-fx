package net.tngou.iask.fx.test;

import java.io.Writer;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.JSONWriter;

import net.tngou.iask.fx.http.HttpUtil;
import net.tngou.iask.fx.http.UrlConfig;
import net.tngou.iask.fx.util.JsonUtil;

public class HttpTest {

	public static void main(String[] args) {
		
		String url=UrlConfig.get("login");
		String json = HttpUtil.getString(url);
	
		System.err.println(JsonUtil.toJSONObject(json+"}"));
	
	}

}

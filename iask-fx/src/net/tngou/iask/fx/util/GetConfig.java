package net.tngou.iask.fx.util;

import java.io.File;

import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * 取得配置
 * @author tngou@tngou.net
 *
 */
public class GetConfig {
   
	private static final Logger log= LoggerFactory.getLogger(GetConfig.class);
    public static PropertiesConfiguration get(String name) {
    	PropertiesConfiguration config= new PropertiesConfiguration();
    	try {
  		  File propertiesFile = new File(name);
  		  Configurations configs = new Configurations();
  		  config = configs.properties(propertiesFile);				
  		} catch (ConfigurationException  e) {		
  			log.error("解析配置文件{}错误",name);
  		}
		return config;
	}
	
	
	
}

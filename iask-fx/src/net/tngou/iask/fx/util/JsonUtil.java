package net.tngou.iask.fx.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;

/**
 * JSON
 * @author tngou@tngou.net
 *
 */
public class JsonUtil {

	/**
	 * 转为JSON对象
	 * @param json
	 * @return
	 */
	public static JSONObject  toJSONObject(String json) {	
		try{
			return JSONObject.parseObject(json);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * 转为JSON对象
	 * @param json
	 * @return
	 */
	public static JSONArray toJSONArray(String json) {
		try{
			return JSONArray.parseArray(json);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 输入输出空值
	 * 在fastjson中，缺省是不输出空值的。无论Map中的null和对象属性中的null，序列化的时候都会被忽略不输出，这样会减少产生文本的大小。
	 * WriteNullListAsEmpty 	将Collection类型字段的字段空值输出为[]
	 * WriteNullStringAsEmpty 	将字符串类型字段的空值输出为空字符串 ""
	 * WriteNullNumberAsZero 	将数值类型字段的空值输出为0
	 * WriteNullBooleanAsFalse 	将Boolean类型字段的空值输出为false
	 * @param object
	 * @return
	 */
	public static String toJSONString(Object object) {
		return JSON.toJSONString(object, 
				SerializerFeature.WriteMapNullValue,
				SerializerFeature.WriteNullListAsEmpty,
				SerializerFeature.WriteNullStringAsEmpty,
				SerializerFeature.WriteNullNumberAsZero,
				SerializerFeature.WriteNullBooleanAsFalse);
	}
	
}

package net.tngou.iask.fx.model;

import java.sql.Date;
import java.sql.Timestamp;

public class Person {
	   private String name;  
	    private int age;  
	    private Timestamp birth;  
	    public String getName() {  
	        return name;  
	    }  
	    public void setName(String name) {  
	        this.name = name;  
	    }  
	    public int getAge() {  
	        return age;  
	    }  
	    public void setAge(int age) {  
	        this.age = age;  
	    }  
	    public Timestamp getBirth() {  
	        return birth;  
	    }  
	    public void setBirth(Timestamp birth) {  
	        this.birth = birth;  
	    }  
}

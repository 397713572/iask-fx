package net.tngou.iask.fx.cache;

import java.io.Serializable;
import java.net.URL;

import org.apache.commons.configuration2.FileBasedConfiguration;
import org.apache.commons.configuration2.XMLConfiguration;
import org.apache.commons.configuration2.builder.FileBasedConfigurationBuilder;
import org.apache.commons.configuration2.builder.fluent.Parameters;
import org.ehcache.Cache;
import org.ehcache.CacheManager;
import org.ehcache.config.CacheRuntimeConfiguration;
import org.ehcache.config.Configuration;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.CacheManagerBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.expiry.Expiry;
import org.ehcache.xml.XmlConfiguration;

/**
 * 基于 Ehchache 缓存
 * @author tngou@tngou.net
 *
 */
public class EhCacheEngine implements CacheEngine {
 private  static CacheManager cacheManager;
	static{
		Parameters params = new Parameters();
		@SuppressWarnings({ "rawtypes", "unchecked" })
		FileBasedConfigurationBuilder<?> builder =
			    new FileBasedConfigurationBuilder(FileBasedConfiguration.class)
			    .configure(params.xml()
			        .setFileName("ehcache.xml"));
		URL url = builder.getFileHandler().getURL();
		Configuration xmlConfig= new XmlConfiguration(url);
		cacheManager = CacheManagerBuilder.newCacheManager(xmlConfig); 
		cacheManager.init();
	}
	@Override
	public  void stop() {
		cacheManager.close();
	}

	@Override
	public void add(String cacheName, Serializable key, Object value) {
		Cache<Serializable, Object> cache=
			    cacheManager.getCache(cacheName, Serializable.class, Object.class);
		if(cache!=null)
		cache.put(key, value);
		
	}

	@Override
	public Object get(String cacheName, Serializable key) {
		Cache<Serializable, Object> cache=
			    cacheManager.getCache(cacheName, Serializable.class, Object.class);
		if(cache==null)return null;
		return cache.get(key);
	}

	

	@Override
	public void remove(String cacheName, Serializable key) {
		Cache<Serializable, Object> cache=
			    cacheManager.getCache(cacheName, Serializable.class, Object.class);
		if(cache!=null)cache.remove(key);
		
	}

	@Override
	public void remove(String cacheName) {
		Cache<Serializable, Object> cache=
			    cacheManager.getCache(cacheName, Serializable.class, Object.class);
		if(cache!=null)cache.clear();
		
	}
	
	
	
}

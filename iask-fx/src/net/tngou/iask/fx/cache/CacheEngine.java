package net.tngou.iask.fx.cache;

import java.io.Serializable;

/**
 * 缓存
 * @author tngou@tngou.net
 *
 */
public interface CacheEngine {
    String cacheName="default";
	public  void add(String cacheName, Serializable key, Object value);
	public  Object get(String cacheName, Serializable key);
	public  void remove(String cacheName, Serializable key) ;
	public  void remove(String cacheName) ;
	public  void stop() ;
}




package net.tngou.iask.fx.http;   


import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



/**
 * Http请求  get与Post
 * @author 陈磊
 *
 */

public class HttpUtil {

	private static final Logger log= LoggerFactory.getLogger(HttpUtil.class);
	private static HttpConfig httpConfig = HttpConfig.getInstance();
	

	public static String getString(String url,Map<String, String> data) {
			
		String json="";
		CloseableHttpClient httpclient = HttpClients.createDefault();  
		List<NameValuePair> nvps = new ArrayList<NameValuePair>();
	    Set<String> set = data.keySet();
	    
		set.forEach(e->{nvps.add(new BasicNameValuePair(e, data.get(e)));});
        try {
	
			URI uri = new URIBuilder(url).setParameters(nvps) .build();
        	// 创建httpget.    
            HttpGet httpget = new HttpGet(uri);  
            httpget.addHeader("User-Agent", httpConfig.getUserAgent());
            RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout( httpConfig.getTimeout()).setConnectTimeout(httpConfig.getTimeout()).build();//设置请求和传输超时时间      
            httpget.setConfig(requestConfig);
           // httpget.s
			CloseableHttpResponse response = httpclient.execute(httpget);
			// 获取响应实体    
            HttpEntity entity = response.getEntity();
     
            json=EntityUtils.toString(entity,"UTF-8");
            response.close(); 
		} catch (IOException | URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally
		{
			try {
				httpclient.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return json;
		
	}

	
	
   public static String postString(String url,Map<String, String> data) {
		
		
		String json=null;
		CloseableHttpClient httpclient = HttpClients.createDefault();  
		List<NameValuePair> nvps = new ArrayList<NameValuePair>();
	    Set<String> set = data.keySet();
	    
		set.forEach(e->{nvps.add(new BasicNameValuePair(e, data.get(e)));});
        try {
        	
        	
			URI uri = new URIBuilder(url).setParameters(nvps) .build();
        	// 创建httpget.    
            HttpPost httppost = new HttpPost(uri);  
            httppost.setEntity(new UrlEncodedFormEntity(nvps));
            httppost.addHeader("User-Agent", httpConfig.getUserAgent());
            RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout( httpConfig.getTimeout()).setConnectTimeout(httpConfig.getTimeout()).build();//设置请求和传输超时时间      
            httppost.setConfig(requestConfig);
           // httpget.s
			CloseableHttpResponse response = httpclient.execute(httppost);
			// 获取响应实体    
            HttpEntity entity = response.getEntity();
     
            json=EntityUtils.toString(entity,"UTF-8");
            response.close(); 
		} catch (IOException | URISyntaxException e) {
		
			e.printStackTrace();
			return null;
		} finally
		{
			try {
				httpclient.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return json;
		
	}
	
	public static String getString(String url) {
				
		String json=null;
		CloseableHttpClient httpclient = HttpClients.createDefault();  

        try {
        	 	
			URI uri = new URIBuilder(url).build();
        	// 创建httpget.    
            HttpGet httpget = new HttpGet(uri);  
            httpget.addHeader("User-Agent", httpConfig.getUserAgent());
            RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout( httpConfig.getTimeout()).setConnectTimeout(httpConfig.getTimeout()).build();//设置请求和传输超时时间      
            httpget.setConfig(requestConfig);
           // httpget.s
			CloseableHttpResponse response = httpclient.execute(httpget);
			// 获取响应实体    
            HttpEntity entity = response.getEntity();
     
            json=EntityUtils.toString(entity,"UTF-8");
            response.close(); 
		} catch (IOException | URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally
		{
			try {
				httpclient.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return json;
		
	}
	
}
  


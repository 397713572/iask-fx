package net.tngou.iask.fx.http;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.tngou.iask.fx.util.GetConfig;

/**
 * URL地址
 * @author tngou@tngou.net
 *
 */
public class UrlConfig {
	private static final Logger log= LoggerFactory.getLogger(UrlConfig.class);
	private static final String ConfigName="url.properties";
	private static PropertiesConfiguration config= new PropertiesConfiguration();

	
	static
	{		
		config= GetConfig.get(ConfigName);		 
	}
	
	public static String  get(String name) {
		if(config.getString("host")==null||config.getString(name)==null)
		{
			log.error("请求的URL地址{}不存在",name);
			return null;
		}		
		return  config.getString("host")+config.getString(name);
	}
	
}
